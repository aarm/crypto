import React from 'react';
import {Layout, Typography, Space, Menu} from "antd";
import {Navbar} from "./components";
import './App.css';
import Routes from "./Routes";
import {Link} from "react-router-dom";
import './components/darkMode.css'
import DarkMode from "./components/darkMode";

const App = () => {

    return (
        <div className='app'>
            <div className='navbar'>
                <Navbar/>
            </div>
            <div className='main'>
                {/*<Layout>*/}
                    <div className='routes'>
                        <Routes/>
                    </div>
                {/*</Layout>*/}
                {/*<div className='footer'>*/}
                {/*    <Typography.Title level={5} style={{color: 'white', textAlign: 'center'}}>*/}
                {/*        Crypto <br/>*/}
                {/*        All rights reserved*/}
                {/*    </Typography.Title>*/}
                {/*    <Space>*/}
                {/*        <Link to='/'>Home</Link>*/}
                {/*        <Link to='/cryptocurrencies'>Cryptocurrencies</Link>*/}
                {/*        <Link to='/News'>News</Link>*/}
                {/*    </Space>*/}
                {/*</div>*/}
            </div>
        </div>
    )
}

export default App;