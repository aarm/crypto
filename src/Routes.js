import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import {Cryptocurrencies, CryptoDetails, Exchanges, Homepage, News} from "./components";

const Routes = ({ setUser, roleId }) => {
    return (
        <Switch>
            <Route exact path='/'>
                <Homepage/>
            </Route>
            <Route exact path='/exchanges'>
                <Exchanges/>
            </Route>
            <Route exact path='/cryptocurrencies'>
                <Cryptocurrencies/>
            </Route>
            <Route exact path='/crypto/:coinId'>
                <CryptoDetails/>
            </Route>
            <Route exact path='/news'>
                <News/>
            </Route>
        </Switch>
    );
};

export default Routes;
