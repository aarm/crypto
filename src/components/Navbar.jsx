import React from 'react';
import {Link} from 'react-router-dom';
import {Button, Menu, Typography, Avatar} from "antd";
import {HomeOutlined, MoneyCollectOutlined, BulbOutlined, FundOutlined, MenuOutlined} from "@ant-design/icons";
import DarkMode from "./darkMode";

const Navbar = () => {
    return (
        <div className='nav-container'>
            <div className='logo-container'>
                <Avatar src='https://i.ibb.co/Z11pcGG/cryptocurrency.png' size='large'/>
                <Typography.Title level={2} className="logo">
                    <Link to='/'>Crytoverse</Link>
                </Typography.Title>
            </div>
            <Menu theme='dark' className='navbar-list'>
                <Menu.Item icon={<HomeOutlined/>}>
                    <Link to='/'>Home</Link>
                </Menu.Item>
                <Menu.Item icon={<FundOutlined/>}>
                    <Link to='/cryptocurrencies'>Cryptocurrencies</Link>
                </Menu.Item>
                {/*<Menu.Item icon={<MoneyCollectOutlined/>}>*/}
                {/*    <Link to='/exchanges'>Exchanges</Link>*/}
                {/*</Menu.Item>*/}
                <Menu.Item icon={<BulbOutlined/>}>
                    <Link to='/news'>News</Link>
                </Menu.Item>
            </Menu>
        </div>
    )
}

export default Navbar;