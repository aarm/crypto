import React, {useState} from 'react';
import {useGetCryptoNewsQuery} from "../services/cryptoNewsApi";
import {Card, Col, Row, Typography, Select} from "antd";
import {Link} from "react-router-dom";
import moment from "moment";
import Avatar from "antd/es/avatar";
import Text from "antd/es/typography/Text";
import {useGetCryptosQuery} from "../services/cryptoApi";
const  {Title} = Typography;
const {Option} = Select

const demoImg = 'http://coinrevolution.com/wp-content/uploads/2020/06/crytponews.jpg'

const News = (simplified) => {
    const count = simplified ? 10 : 100;
    const [newsCategory, setNewsCategory] = useState('Cryptocurrency')
    const {data: cryptoNews, isFetching} = useGetCryptoNewsQuery({newsCategory , count: simplified ? 20 : 12})
    const {data} = useGetCryptosQuery(100)
    return (
        <Row gutter={[24, 24]} className='crypto-card-container'>
            {
                <Col span={24}>
                    <Select
                    showSearch
                    className='select-news'
                    placeholder='Select a Crypto'
                    optionFilterProp='children'
                    onChange={(value) => setNewsCategory(value)}
                    filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                        <Option value='Cryptocurrency'>Cryptocurrency</Option>
                        {data?.data?.coins.map((coin) => <Option value={coin.name}>{coin.name}</Option>)}
                    </Select>
                </Col>
            }
            {cryptoNews?.value?.map((news, i) => (
                    <Col xs={24} sm={12} lg={8}  key={i}>
                            <Card  className='news-card'
                                  hoverable
                            >
                             <a href={news.url} target="_blank" rel='noreferrer'>
                                 <div className='news-image-container'>
                                     <Title className='news-title' level={4}>{news.name}</Title>
                                     <img style={{maxWidth: '200px', maxHeight: '100px'}} src={news?.image?.thumbnail?.contentUrl || demoImg} alt='news'/>
                                 </div>
                                 <p>
                                     {news.description > 100 ? `${news.description.substring(0, 10)}....`  : news.description}
                                 </p>
                                 <div className='provider-container'>
                                     <div>
                                         <Avatar src={news.provider[0]?.image?.thumbnail?.contentUrl || demoImg}/>
                                         <Text className='provider-name'>{news.provider[0]?.name}</Text>
                                     </div>
                                     <Text>{moment(news.datePublished).startOf('ss').fromNow()}</Text>
                                 </div>
                             </a>
                            </Card>
                    </Col>
                )
            )}
        </Row>
    )
}

export default News;